from distutils.core import setup, Extension
import numpy
from Cython.Distutils import build_ext
from Cython.Compiler.Options import get_directive_defaults

get_directive_defaults()['linetrace'] = True
get_directive_defaults()['binding'] = True

extensions = [
    Extension("test", ["test.pyx"], define_macros=[('CYTHON_TRACE', '1')],
              )
]

setup(
    cmdclass={'build_ext': build_ext},
    ext_modules=[Extension("CCBuilder_c", sources=["CCBuilder_c.pyx"], include_dirs=[numpy.get_include()],
                           extra_compile_args=["-g"],
                           extra_link_args=["-g"]
                           )]
)
