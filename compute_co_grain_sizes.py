#!/usr/bin/env python3
from __future__ import print_function
from __future__ import division
import argparse
import numpy as np
import CCBuilder as ccb

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                 description='''Computes Co grain sizes.
Read a previously written Dream3D file.
Uses flood fill algorithm to find all continuously connected regions in the Co matrix.
Computations account for periodicity, but will typically find many small regions of 1-2 voxels stemming from 
discretization errors.
''')

parser.add_argument('-f', dest='fname', metavar='basename', required=True, help='Input/output base filename.')

options = parser.parse_args()

fname = options.fname

M, spacing, _, phases, _, _, _ = ccb.read_dream3d(fname)

co_grain_sizes, co_ids = ccb.compute_co_grain_sizes(M, phases)

# Skip everything 4 voxels or smaller:
top_sizes = co_grain_sizes[co_grain_sizes >= co_grain_sizes[5]] / np.sum(co_grain_sizes)
print('Top Co grain sizes (normalized):', ', '.join(['{:.3f}'.format(x) for x in top_sizes]))

np.savetxt(fname + '_co_grain_sizes.txt', co_grain_sizes)
