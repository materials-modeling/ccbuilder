#!/usr/bin/env python3
from __future__ import print_function
from __future__ import division
import numpy as np
import CCBuilder as ccb
import CCBuilder_c as ccb_c
from misorientation import *
import sys


# Function for creating uniform distributions of given range
def uniform_dist(x):
    return lambda : np.random.uniform(x[0], x[1])


def weibull_dist(a, mu):
    return lambda : np.random.weibull(a) * mu

# Controlled randomness (for repeatability)
np.random.seed(0)

# Cube size:
L = float(10)

# Grain size:
d_0 = 1  # 1 micron average grain size (length scale) (should reflect the average grain size)
d_eq = uniform_dist([d_0 - 0.5, d_0 + 0.5])
# d_eq = uniform_dist([d_0, d_0])
# d_eq = weibull_dist(4, d_0)

# Grain shape:
r = uniform_dist([0.1, 0.4])
# k = uniform_dist([0.1, 0.4])  # low k (C rich)
k = uniform_dist([0.4, 1.4])  # high k (W rich)

# Target volume fraction:
vol_frac_goal = 1.0

# Final discretization density:
m = 20  # given as "voxels per d_0"
# Disretization to use for packing (somewhere around 5 to 10 works fine for the default grain size).
m_coarse = 10  # given as "voxels per d_0"

# MC steps (scaled with M^4)
mc_stepss = [0., 0.0025, 0.005, 0.01, 0.025, 0.05, 0.1]
# Fictitious temperature for Potts
kBT = 0.5

# Number of random translations to try
nr_tries = int(sys.argv[1])
use_potential = bool(int(sys.argv[2]))

contiguity_2 = []
vol_frac_Co_0 = []
d_eq_mean = []
d_eq_std = []

# if use_potential:
#     vol_frac_goal = 1.08
# elif nr_tries > 1:
#     vol_frac_goal = 1.0
# else:
#     vol_frac_goal = 2.05

print("Running vol fraction", vol_frac_goal)

# to avoid confusion
vol_frac_goal = np.double(vol_frac_goal)
L = np.double(L)
kBT = np.double(kBT)
nr_tries = np.int(nr_tries)
M = np.int(m * L / d_0)
M_coarse = np.int(m_coarse * L / d_0)

trunc_triangles = ccb.prepare_triangles(vol_frac_goal, L, r, k, d_eq)
trunc_triangles.sort(key=lambda m: m.volume, reverse=True)
if use_potential:
    ccb.optimize_midpoints(L, trunc_triangles)
if nr_tries > 1:
    grain_ids_coarse_0, overlaps_coarse_0, voxel_indices_coarse_0 = ccb_c.populate_voxels(M_coarse, L, trunc_triangles, nr_tries, M_coarse, 1.0)

grain_ids_0, overlaps_0, voxel_indices_0 = ccb_c.populate_voxels(M, L, trunc_triangles, 1, 0, 1.0)
phases_0, good_voxels_0, euler_angles_0, phase_volumes_0, grain_volumes_0 = ccb_c.calc_grain_prop(M, grain_ids_0, trunc_triangles)
surface_voxels_0, gb_voxels_0, interface_voxels_0 = ccb_c.calc_surface_prop(M, grain_ids_0)

vol_frac_WC_0 = phase_volumes_0[1]/np.float(np.sum(phase_volumes_0))
vol_frac_Co_0.append( 1 - vol_frac_WC_0 )

print("********************** Final vol fraction", 1 - vol_frac_Co_0[-1])

for mc_steps in mc_stepss:
    print("Running", mc_steps, "MC steps")
    # Do Potts on coarse grid first for an improved initial guess.
    grain_ids_2 = grain_ids_0.copy()
    M_coarse = M//2
    grain_ids_coarse, overlaps_coarse, voxel_indices_coarse = ccb_c.populate_voxels(M_coarse, L, trunc_triangles, 0, M_coarse, 1.0)
    surface_voxels_coarse, gb_voxels_coarse, interface_voxels_coarse = ccb_c.calc_surface_prop(M_coarse, grain_ids_coarse)
    ccb_c.make_mcp_bound(M_coarse, grain_ids_coarse, gb_voxels_coarse, overlaps_coarse, voxel_indices_coarse, np.int(mc_steps*M_coarse**4), kBT)
    #
    # Copy over that solution to the overlap regions of the fine grid as a starting point
    i = np.nonzero(overlaps_0)[0]
    iz = i // (M*M)
    iy = (i - iz*M*M) // M
    ix = i - iz*M*M - iy*M
    cix, ciy, ciz = ix * M_coarse // M, iy * M_coarse // M, iz * M_coarse // M
    ci = cix + ciy*M_coarse + ciz*M_coarse**2
    # For a very coarse grid, we should check the voxel indices. Requires Cython implementation for efficiency.
    gid = grain_ids_coarse[ci]
    for ii, g in zip(i, gid):
        if g != grain_ids_2[ii] and np.searchsorted(voxel_indices_0[g-2], ii) < len(voxel_indices_0[g-2]):
            grain_ids_2[ii] = g
    # This might change a few voxels to a value that they shouldn't obtain, but it's barely noticeable
    #grain_ids_2[i] = grain_ids_coarse[ci]
    surface_voxels_2, gb_voxels_2, interface_voxels_2 = ccb_c.calc_surface_prop(M, grain_ids_2)
    ccb_c.make_mcp_bound(M, grain_ids_2, gb_voxels_2, overlaps_0, voxel_indices_0, np.int(mc_steps*M**4), kBT)

    sum_gb_voxels_2 = np.sum(gb_voxels_2)
    contiguity_2.append( sum_gb_voxels_2 / np.float(sum_gb_voxels_2 + np.sum(interface_voxels_2)) )
    print("Contiguity is at", contiguity_2[-1])

    phases_2, good_voxels_2, euler_angles_2, phase_volumes_0, grain_volumes_2 = ccb_c.calc_grain_prop(M, grain_ids_2, trunc_triangles)
    d_eq_mean.append(np.mean(np.cbrt(6./np.pi * grain_volumes_2 * ((L/M)**3))))
    d_eq_std.append(np.std(np.cbrt(6./np.pi * grain_volumes_2 * ((L/M)**3))))

if False:
    import matplotlib.pyplot as plt
    plt.plot(np.array(mc_stepss), contiguity_2)
    plt.show()

fname = 'stat_mc_steps'
if use_potential:
    fname += '_U'
elif nr_tries == 0:
    fname += '_random'
else:
    fname += '_transl'

np.savetxt(fname + "_{}_L{}.txt".format(m, L), np.transpose([np.array(mc_stepss), contiguity_2, d_eq_mean, d_eq_std]),
           header='mc_steps  contiguity  d_eq_mean  d_eq_std',
           comments='')
