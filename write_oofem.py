#!/usr/bin/env python3
from __future__ import print_function
from __future__ import division
import CCBuilder as ccb
import argparse

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                 description='''Writes input files for OOFEM analysis.
Read a previously written Dream3D file.
''')

parser.add_argument('-f', dest='fname', metavar='basename', required=True, help='Input/output base filename.')

parser.add_argument('--stress', action='store_true',
                    help='Write OOFEM input file for residual stress analysis')
parser.add_argument('--wc_elast', metavar='elast', default='Golovchan', type=str,
                    help='WC elastic parameters (Lee, Golovchan, Suetin, Johansson).')
parser.add_argument('--wc_alpha', metavar='alpha', default='Mari', type=str,
                    help='WC alpha parameter (Mari, Touloukian, Cheng).')
parser.add_argument('--transport', action='store_true',
                    help='Write OOFEM input file for transport problem for Co-matrix')

options = parser.parse_args()

if not (options.transport or options.stress):
    print('Nothing to write. Request at least one of stress or transport.')
    exit(-1)

fname = options.fname

M, spacing, grain_ids, phases, _, euler_angles, _ = ccb.read_dream3d(fname)

# TODO: compute truncated triangels from arbitrary (non-ccbuilder)
# Dream3D dataset (based on euler angles + center of mass)
# Until then, restore the pickled data:
with open(fname + '_trunc_triangles.data', 'rb') as f:
    import pickle
    trunc_triangles = pickle.load(f)

if options.stress:
    ccb.write_oofem(fname, M, spacing, trunc_triangles, grain_ids, options.wc_alpha, options.wc_elast)
if options.transport:
    ccb.write_oofem_transport(fname + "_d", M, spacing, phases)
