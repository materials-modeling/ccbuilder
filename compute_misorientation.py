#!/usr/bin/env python3
from __future__ import print_function
from __future__ import division
import CCBuilder as ccb
from misorientation import compute_all_misorientation_voxel
import numpy as np
import argparse

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                 description='''Writes all misorientations to a text file.
Read a previously written Dream3D file.
''')

parser.add_argument('-f', dest='fname', metavar='basename', required=True, help='Input/output base filename.')

options = parser.parse_args()

fname = options.fname

M, _, grain_ids, _, _, euler_angles, _ = ccb.read_dream3d(fname)

# TODO: compute truncated triangels from arbitrary (non-ccbuilder)
# Dream3D dataset (based on euler angles + center of mass)
# Until then, restore the pickled data:
with open(fname + '_trunc_triangles.data', 'rb') as f:
    import pickle
    trunc_triangles = pickle.load(f)

angles = compute_all_misorientation_voxel(trunc_triangles, grain_ids, M)

all_angles = np.array(list(angles.values()))
np.savetxt(fname + '_misorientation.txt', all_angles)
