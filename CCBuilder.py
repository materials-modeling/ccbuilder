from __future__ import print_function
import numpy as np
import numpy.linalg as la
import GeometryTools
import TruncatedTriangle as tt
import scipy.optimize
import CCBuilder_c as ccb_c


def prepare_triangles(vol_frac_goal, L, r_d, k_d, d_eq_d):
    """
    Prepares a list of truncated prisms from the given distributions.
    :param vol_frac_goal: Goal volume fraction.
    :param L: Size of window.
    :param r_d: Distribution (callback function) for r, truncation size.
    :param k_d: Distribution (callback function) for k, elongation factor.
    :param d_eq_d: Distribution (callback function) for d, equivalent diameter.
    :return: List of prisms which contain random locations and a random r, k, and d.
    """
    print("Prepare triangles")

    trunc_triangles = list()

    total_volume = L**3
    volume = 0.

    # Make random grains
    while volume < vol_frac_goal * total_volume:
        midpoint = L * np.random.random(3)
        r = r_d()
        k = k_d()
        d_eq = d_eq_d()

        rot_matrix = GeometryTools.random_rotation()

        trunc_triangle = tt.TruncatedTriangle(midpoint, rot_matrix, r, k, d_eq)

        trunc_triangles.append(trunc_triangle)

        volume += trunc_triangle.volume

    return trunc_triangles


def optimize_midpoints(L, trunc_triangles):
    """
    Minimizes overlap based on simplified grain geometries
    :param L: Window size
    :param trunc_triangles: List of truncated prisms (modified on output)
    """
    N = len(trunc_triangles)

    print(f"Optimizing midpoints of {N} grains.")

    x0 = np.ndarray.flatten(np.array([trunc_triangle.midpoint for trunc_triangle in trunc_triangles]))
    # Radius the use for penetration approximation:
    #r0 = np.array([trunc_triangle.circumcircle for trunc_triangle in trunc_triangles])
    # Using the radius of the (volume) equivalent sphere gives good packing:
    r0 = np.array([trunc_triangle.d_eq*0.5 for trunc_triangle in trunc_triangles])
    volumes = np.array([trunc_triangle.volume for trunc_triangle in trunc_triangles])

    result = scipy.optimize.minimize(ccb_c.sum_potential3_and_grad, x0, args=(L, r0, volumes), method='l-bfgs-b', jac=True, tol=1E-4, options = {'disp' : True, 'maxiter' : 50})

    for i in range(N):
        trunc_triangles[i].set_midpoint(np.mod(result.x[3*i:3*i+3], L))


def mass_fraction(vol_frac):
    density_WC = 15.63  # g/cm3
    density_Co = 8.90  # g/cm3
    return vol_frac*density_WC / (vol_frac*density_WC + (1-vol_frac)*density_Co)


def volume_to_eq_d(volume):
    return np.cbrt(6./np.pi * volume)


def compute_co_grain_sizes(M, grain_ids):
    """
    Computes the Co grain connectivity (important parameter for mass transport through the Co matrix)
    by counting the unique grains
    :param M: Voxel discretization parameter
    :param grain_ids: Array of all voxel grain IDs, where grain 1 is the Co phase
    :return: Co grain size distribution
    """
    import scipy.ndimage
    # We also consider connectivity across the diagonals
    structure = np.ones((3, 3, 3))
    labels, numL = scipy.ndimage.label(np.reshape(grain_ids == 1, M), structure=structure)
    # since labels aren't computed periodically, we have to fix this:
    for i in range(M[0]):
        for j in range(M[1]):
            if labels[i][j][0] != 0 and labels[i][j][-1] != 0 and labels[i][j][0] != labels[i][j][-1]:
                labels[labels == labels[i][j][-1]] = labels[i][j][0]
    for i in range(M[0]):
        for k in range(M[2]):
            if labels[i][0][k] != 0 and labels[i][-1][k] != 0 and labels[i][0][k] != labels[i][-1][k]:
                labels[labels == labels[i][-1][k]] = labels[i][0][k]
    for j in range(M[1]):
        for k in range(M[2]):
            if labels[0][j][k] != 0 and labels[-1][j][k] != 0 and labels[0][j][k] != labels[-1][j][k]:
                labels[labels == labels[-1][j][k]] = labels[0][j][k]

    d = np.bincount(labels[labels != 0])
    d.sort()
    d = d[::-1]
    d = d[d > 0]
    return d, np.reshape(labels, M[0]*M[1]*M[2])


def write_oofem(filename, M, spacing, trunc_triangles, grain_ids, wc_alpha, wc_elast):
    """
    Exports OOFEM input file for elasticity analysis. Preset for thermal residual stress analysis.
    """
    f = open(filename + ".in", "w")

    ids_in_grain = [list() for _ in range(max(grain_ids))]
    for el, grain in enumerate(grain_ids):
        ids_in_grain[grain-1].append(el)
    nmat = len(ids_in_grain)

    NX = M[0]+1
    NY = M[1]+1
    NZ = M[2]+1

    f.write("{}.out\n".format(filename))
    f.write("Generated structured grid for {}\n".format(filename))
    f.write("StaticStructural nsteps 8 deltat 1 nmodules 1 lstype 3 smtype 7 manrmsteps 1 miniter 1 rtolf 1e-6\n")

    f.write("vtkxml tstep_all cellvars 6 1 4 81 103 117 27 primvars 1 1 stype 0\n")
    f.write("domain 3d\n")
    f.write("outputmanager\n")
    f.write("ndofman {0} nelem {1} nset {2} ncrosssect {3} nmat {3} nbc {4} nltf {5} nic 0\n".format(
        NX*NY*NZ, M[0]*M[1]*M[2], nmat+5, nmat, 2, 2))
    node = 0
    for nz in range(NX):
        for ny in range(NY):
            for nx in range(NZ):
                node += 1
                f.write("Node {} coords 3 {} {} {}\n".format(node, nx*spacing[0], ny*spacing[1], nz*spacing[2]))

    element = 0
    for ez in range(M[2]):
        for ey in range(M[1]):
            for ex in range(M[0]):
                element += 1
                nodes = np.array([NX*NY+NZ, NX*NY+NX+1, NX*NY + 1, NY*NX, NX, NX+1, 1, 0]) + (1+ex + NX*ey + NX*NY*ez)
                # Forced to do temperature changes as "body loads" FIXME
                f.write("LSpace {} nodes 8 {} {} {} {} {} {} {} {} bodyloads 1 2\n".format(element, *nodes))

    for setcount, grain in enumerate(ids_in_grain):
        f.write("Set {} elements {}".format(setcount+1, len(grain)))
        f.write("".join(" {}".format(k+1) for k in grain))
        f.write("\n")
    f.write("Set {} nodes {}".format(nmat+1, NX*NY))
    for ny in range(NX):
        for nx in range(NY):
            f.write(" {}".format(nx + NX*ny + 1))
    f.write("\n")
    f.write("Set {} nodes {}".format(nmat+2, NX*NY))
    for ny in range(NX):
        for nx in range(NY):
            f.write(" {}".format(nx + NX*ny + 1 + (NZ-1)*NX*NY))
    f.write("\n")

    f.write("Set {} elementboundaries {}".format(nmat+3, 2*(M[0]*M[1] + M[0]*M[2] + M[1]*M[2])))
    # X-Y plane (facing -z):
    sz = 0
    for sy in range(M[1]):
        for sx in range(M[0]):
            f.write(" {} 2".format(M[0]*M[1]*sz + M[0]*sy + sx + 1))
    # X-Z plane (facing -y):
    for sz in range(M[2]):
        sy = 0
        for sx in range(M[0]):
            f.write(" {} 5".format(M[0]*M[1]*sz + M[0]*sy + sx + 1))
    # Y-Z plane (facing -x):
    for sz in range(M[2]):
        for sy in range(M[1]):
            sx = 0
            f.write(" {} 6".format(M[0]*M[1]*sz + M[0]*sy + sx + 1))
    f.write("\n")

    f.write("Set {} elementboundaries {}".format(nmat+4, 2*(M[0]*M[1] + M[0]*M[2] + M[1]*M[2])))
    # X-Y plane (facing +z):
    sz = M[2]-1
    for sy in range(M[1]):
        for sx in range(M[0]):
            f.write(" {} 1".format(M[0]*M[1]*sz + M[0]*sy + sx + 1))
    # X-Z plane (facing +y):
    for sz in range(M[2]):
        sy = M[1]-1
        for sx in range(M[0]):
            f.write(" {} 3".format(M[0]*M[1]*sz + M[0]*sy + sx + 1))
    # Y-Z plane (facing +x):
    for sz in range(M[2]):
        for sy in range(M[1]):
            sx = M[0]-1
            f.write(" {} 4".format(M[0]*M[1]*sz + M[0]*sy + sx + 1))
    f.write("\n")

    f.write("Set {} noderanges {{(1 {})}}\n".format(nmat+5, NX*NY*NZ))

    for setcount, dummy in enumerate(ids_in_grain):
        f.write("SimpleCS {0} material {0} set {0}\n".format(setcount + 1))

    # D_voigtN = np.zeros((6, 6))
    # D_reussN = np.zeros((6, 6))
    # a_voigtN = np.zeros(6)
    # a_reussN = np.zeros(6)

    # Material parameters (20-800C average):
    if wc_alpha == 'Mari':
        a1, a3 = (5.1e-6, 4.5e-6)  # Mari (2009) (20-800C average)
    elif wc_alpha == 'Touloukian':
        a1, a3 = (4.8e-6, 4.2e-6)  # Touloukian (1977) (20-800C average)
    elif wc_alpha == 'Cheng':
        a1, a3 = (5.2e-6, 4.9e-6)  # Cheng (2012) (20-800C average)
    elif wc_alpha == 'Sayers':
        a1, a3 = (6.7e-6, 4.9e-6)  # Sayers (1987) / Rudiger (1967)
    elif wc_alpha == 'Kim':
        a1, a3 = (5.2e-6, 7.3e-6)  # Kim (2006)
    else:
        raise ValueError('Unknown alpha')

    if wc_elast == 'Lee':
        C11, C12, C13, C33, C44 = (720e9, 254e9, 267e9, 972e9, 328e9)  # Lee (1982)
    elif wc_elast == 'Golovchan':
        C11, C12, C13, C33, C44 = (720e9, 254e9, 150e9, 972e9, 328e9)  # V.T. Golovchan (2010)
    elif wc_elast == 'Johansson':
        C11, C12, C13, C33, C44 = (697e9, 255e9, 187e9, 954e9, 311e9)  # Johansson et al. (2015)
    elif wc_elast == 'Suetin':
        C11, C12, C13, C33, C44 = (772.8e9, 209.9e9, 157.8e9, 960.6e9, 302.2e9)  # Suetin et al. (2008)
    elif wc_elast == 'Li':
        C11, C12, C13, C33, C44 = (711.6e9, 240.6e9, 168.0e9, 977.5e9, 305.1e9)  # Li et al. (2010)
    else:
        raise ValueError('Unknown elastic parameters')
    C66 = 0.5 * (C11-C12)

    # Cobalt phase:
    # a_Co = 13.8e-6
    a_Co = 14.3e-6  # Touloukian (1977) (20-800C average)
    E = 206e9
    G = 78.5e9
    nu = E/(2*G)-1
    K = E*G/(3*(3*G-E))
    # K = E/(3*(1-2*nu))
    # G = E/(2*(1+nu))
    H = 10e9
    sigY = 350e6

    C = np.matrix([[C11, C12, C13, 0, 0, 0],
                   [C12, C11, C13, 0, 0, 0],
                   [C13, C13, C33, 0, 0, 0],
                   [0, 0, 0, C44, 0, 0],
                   [0, 0, 0, 0, C44, 0],
                   [0, 0, 0, 0, 0, C66]])

    """
    Ec = 1.5*206e9
    Gc = 1.5*78.5e9
    nuc = Ec/(2*Gc)-1
    Kc = Ec*Gc/(3*(3*Gc-Ec))
    C = np.matrix([[4/3 * Gc + Kc, -2/3 * Gc + Kc, -2/3 * Gc + Kc, 0, 0, 0],
           [-2/3 * Gc + Kc, 4/3 * Gc + Kc, -2/3 * Gc + Kc, 0, 0, 0],
           [-2/3 * Gc + Kc, -2/3 * Gc + Kc, 4/3 * Gc + Kc, 0, 0, 0],
           [0, 0, 0, Gc, 0, 0],
           [0, 0, 0, 0, Gc, 0],
           [0, 0, 0, 0, 0, Gc]])
    """
    a = [a1, a1, a3, 0, 0, 0]

    D_Co = np.matrix([[4/3 * G + K, -2/3 * G + K, -2/3 * G + K, 0, 0, 0],
                      [-2/3 * G + K, 4/3 * G + K, -2/3 * G + K, 0, 0, 0],
                      [-2/3 * G + K, -2/3 * G + K, 4/3 * G + K, 0, 0, 0],
                      [0, 0, 0, G, 0, 0],
                      [0, 0, 0, 0, G, 0],
                      [0, 0, 0, 0, 0, G]])

    for setcount, grain in enumerate(ids_in_grain):
        if setcount == 0:

            f.write("IsoLE {} d 1. tAlpha {} E {} n {}\n".format(setcount + 1, a_Co, E, nu))
            f.write("#TutorialMaterial {} d 1. tAlpha {} E {} n {} H {} sigy {}\n".format(setcount+1,
                a_Co, E, nu, H, sigY))

            # D_voigtN += D_Co * len(grain)
            # a_voigtN += a_Co * np.array(np.dot(D_Co, [1, 1, 1, 0, 0, 0]))[0] * len(grain)
            # D_reussN += la.inv(D_Co) * len(grain)
            # a_reussN += a_Co * np.array([1, 1, 1, 0, 0, 0]) * len(grain)

        else:

            m = trunc_triangles[setcount-1].rot_matrix  # set = 0 is the matrix material, therefor setcount-1
            x = m[:, 0]
            y = m[:, 1]
            z = m[:, 2]

            Q = np.matrix([
                [x[0] * x[0], x[1] * x[1], x[2] * x[2], x[1] * x[2], x[0] * x[2], x[0] * x[1]],
                [y[0] * y[0], y[1] * y[1], y[2] * y[2], y[1] * y[2], y[0] * y[2], y[0] * y[1]],
                [z[0] * z[0], z[1] * z[1], z[2] * z[2], z[1] * z[2], z[0] * z[2], z[0] * z[1]],
                [2 * y[0] * z[0], 2 * y[1] * z[1], 2 * y[2] * z[2], y[2] * z[1] + y[1] * z[2], y[2] * z[0] + y[0] * z[2], y[1] * z[0] + y[0] * z[1]],
                [2 * x[0] * z[0], 2 * x[1] * z[1], 2 * x[2] * z[2], x[2] * z[1] + x[1] * z[2], x[2] * z[0] + x[0] * z[2], x[1] * z[0] + x[0] * z[1]],
                [2 * x[0] * y[0], 2 * x[1] * y[1], 2 * x[2] * y[2], x[2] * y[1] + x[1] * y[2], x[2] * y[0] + x[0] * y[2], x[1] * y[0] + x[0] * y[1]]
            ])

            D = Q.transpose() * C * Q
            # aRot = np.array(np.dot(Q.transpose(), a))[0]

            aRot = np.array([
                a[0] * x[0] * x[0] + 2 * a[5] * x[0] * y[0] + a[1] * y[0] * y[0] + ( a[4] * x[0] + a[3] * y[0] ) * z[0] + a[2] * z[0] * z[0],
                a[0] * x[1] * x[1] + 2 * a[5] * x[1] * y[1] + a[1] * y[1] * y[1] + ( a[4] * x[1] + a[3] * y[1] ) * z[1] + a[2] * z[1] * z[1],
                a[0] * x[2] * x[2] + 2 * a[5] * x[2] * y[2] + a[1] * y[2] * y[2] + ( a[4] * x[2] + a[3] * y[2] ) * z[2] + a[2] * z[2] * z[2],
                y[2] * ( a[5] * x[1] + 2*a[1] * y[1] + a[3] * z[1] ) + x[2] * ( 2*a[0] * x[1] + a[5] * y[1] + a[4] * z[1] ) + ( a[4] * x[1] + a[3] * y[1] + 2*a[2] * z[1] ) * z[2],
                y[2] * ( a[5] * x[0] + 2*a[1] * y[0] + a[3] * z[0] ) + x[2] * ( 2*a[0] * x[0] + a[5] * y[0] + a[4] * z[0] ) + ( a[4] * x[0] + a[3] * y[0] + 2*a[2] * z[0] ) * z[2],
                y[1] * ( a[5] * x[0] + 2*a[1] * y[0] + a[3] * z[0] ) + x[1] * ( 2*a[0] * x[0] + a[5] * y[0] + a[4] * z[0] ) + ( a[4] * x[0] + a[3] * y[0] + 2*a[2] * z[0] ) * z[1]
            ])

            aDeRot_c = z[0] * z[0] * aRot[0] + \
                       z[1] * z[1] * aRot[1] + \
                       z[2] * z[2] * aRot[2] + \
                       z[1] * z[2] * aRot[3] + \
                       z[0] * z[2] * aRot[4] + \
                       z[0] * z[1] * aRot[5]

            s_test = np.array([1., 1., 1., 0., 0., 0.])
            e_test = np.array(np.dot(la.inv(D), s_test))[0]

            e_c = z[0] * z[0] * e_test[0] + \
                  z[1] * z[1] * e_test[1] + \
                  z[2] * z[2] * e_test[2] + \
                  z[1] * z[2] * e_test[3] + \
                  z[0] * z[2] * e_test[4] + \
                  z[0] * z[1] * e_test[5]

            e_a = x[0] * x[0] * e_test[0] + \
                  x[1] * x[1] * e_test[1] + \
                  x[2] * x[2] * e_test[2] + \
                  x[1] * x[2] * e_test[3] + \
                  x[0] * x[2] * e_test[4] + \
                  x[0] * x[1] * e_test[5]

            e_b = y[0] * y[0] * e_test[0] + \
                  y[1] * y[1] * e_test[1] + \
                  y[2] * y[2] * e_test[2] + \
                  y[1] * y[2] * e_test[3] + \
                  y[0] * y[2] * e_test[4] + \
                  y[0] * y[1] * e_test[5]

            Dsym = [D[0, 0], D[0, 1], D[0, 2], D[0, 3], D[0, 4], D[0, 5],
                    D[1, 1], D[1, 2], D[1, 3], D[1, 4], D[1, 5],
                    D[2, 2], D[2, 3], D[2, 4], D[2, 5],
                    D[3, 3], D[3, 4], D[3, 5],
                    D[4, 4], D[4, 5],
                    D[5, 5]]

            # Compute the Voigt and Reuss bounds
            # D_voigtN += D * len(grain)
            # a_voigtN += np.array(np.dot(D, aRot))[0] * len(grain)
            # D_reussN += la.inv(D) * len(grain)
            # a_reussN += aRot * len(grain)
            f.write("AnIsoLE {} d 1. ".format(setcount+1))
            f.write("tAlpha 6 {0[0]} {0[1]} {0[2]} {0[3]} {0[4]} {0[5]} ".format(aRot))
            f.write(("stiff 21 {0[0]} {0[1]} {0[2]} {0[3]} {0[4]} {0[5]} {0[6]} {0[7]} {0[8]} {0[9]} {0[10]} " +
                    "{0[11]} {0[12]} {0[13]} {0[14]} {0[15]} {0[16]} {0[17]} {0[18]} {0[19]} {0[20]}\n").format(Dsym))
            #f.write("IsoLE {} d 1. tAlpha {} E {} n {}\n".format(setcount+1, 6.2e-6, 719e9, 0.19))

    if True:
        v_Co = 0.18  # len(ids_in_grain[0]) / (M[0]*M[1]*M[2])

        Cinv = la.inv(C)
        # DCoinv = la.inv(D_Co)
        K_voigt = 1/9 * (C[0, 0] + C[0, 1] + C[0, 2] +
                         C[1, 0] + C[1, 1] + C[1, 2] +
                         C[2, 0] + C[2, 1] + C[2, 2]) * (1 - v_Co) + K * v_Co
        K_reuss = 1/(
            (Cinv[0, 0] + Cinv[0, 1] + Cinv[0, 2] +
             Cinv[1, 0] + Cinv[1, 1] + Cinv[1, 2] +
             Cinv[2, 0] + Cinv[2, 1] + Cinv[2, 2]) * (1 - v_Co) +
            1/K * v_Co)

        G_voigt = 1/16 * (2*C[0, 0]/3 - C[0, 1]/3 - C[0, 2]/3 +
                          -C[1, 0]/3 + 2*C[1, 1]/3 - C[1, 2]/3 +
                          -C[2, 0]/3 - C[2, 1]/3 + 2*C[2, 2]/3 +
                          4*(C[3, 3] + C[4, 4] + C[5, 5])) * (1 - v_Co) + G * v_Co
        G_reuss = 1/(
            1/4 * (2*Cinv[0, 0]/3 - Cinv[0, 1]/3 - Cinv[0, 2]/3 +
             - Cinv[1, 0]/3 + 2*Cinv[1, 1]/3 - Cinv[1, 2]/3 +
             - Cinv[2, 0]/3 - Cinv[2, 1]/3 + 2*Cinv[2, 2]/3 +
            1*(Cinv[3, 3] + Cinv[4, 4] + Cinv[5, 5])) * (1 - v_Co) +
            1/G * v_Co)

        a_voigt = ((1 - v_Co) * (2*(C11+C12+C13)*a1 + (2*C13+C33)*a3) + v_Co * 9 * K * a_Co) / (9*K_voigt)
        a_reuss = ((1 - v_Co) * (2*a1 + a3) + v_Co * a_Co * 3) / 3

        print("K_voigt =", K_voigt, "K_reuss =", K_reuss)
        print("G_voigt =", G_voigt, "G_reuss =", G_reuss)
        print("a_voigt =", a_voigt, "a_reuss =", a_reuss)

        # D_voigtN = D_voigtN / (M[0]*M[1]*M[2])
        # D_reussN = la.inv(D_reussN / (M[0]*M[1]*M[2]))
        # a_voigtN = np.array(np.dot(la.inv(D_voigtN), a_voigtN)) / (M[0]*M[1]*M[2])
        # a_reussN = a_reussN / (M[0]*M[1]*M[2])
        # print("D_voigt =", D_voigtN)
        # print("D_reuss =", D_reussN)
        # print("a_voigt =", a_voigtN)
        # print("a_reuss =", a_reussN)

    # f.write("PrescribedGradient 1 loadTimeFunction 1 dofs 3 1 2 3" +
    #         " gradient 3 3 {{0. 0. 0.; 0. 0. 0.; 0. 0. 0.}} set {}\n".format(nmat+3))
    #         " gradient 3 3 {{{0} 0. 0.; 0. {1} 0.; 0. 0. {2}}}".format(-800 * (a_reuss+a_voigt)/2) +
    f.write("PrescribedGradientPeriodic 1 loadTimeFunction 1 dofs 3 1 2 3" +
            " gradient 3 3 {{{0} 0. 0.; 0. {1} 0.; 0. 0. {2}}}".format(0, 0, 0) +
            " jump 3 {0} {1} {2} set {3} masterSet {4}\n".format(spacing[0]*M[0], spacing[0]*M[1], spacing[0]*M[2],
                                                                 nmat+4, nmat+3))
    f.write("StructTemperatureLoad 2 loadTimeFunction 2 components 2 {} 0.0\n".format(1.0))
    f.write("ConstantFunction 1 f(t) 1.0\n")
    f.write("#Piecewiselinfunction 2 t 2 0. 1. f(t) 2 0. 1.\n")
    f.write("Piecewiselinfunction 2 t 9 0 1 2 3 4 5 6 7 8 f(t) 9 0 -100 -200 -300 -400 -500 -600 -700 -780\n")


def write_oofem_transport(filename, M, spacing, phase_ids):
    """
    Exports an OOFEM input file for mass/heat transport analysis in the Co phase.
    """
    f = open(filename + ".in", "w")

    ids_in_phase = [list() for _ in range(max(phase_ids))]
    for el, grain in enumerate(phase_ids):
        ids_in_phase[grain-1].append(el)
    nmat = len(ids_in_phase)

    k_Co = 1.0
    k_WC = 0.0

    NX = M[0]+1
    NY = M[1]+1
    NZ = M[2]+1

    f.write("{}.out\n".format(filename))
    f.write("Generated heat/mass transport grid for {}\n".format(filename))
    f.write("StationaryProblem nsteps 1 nmodules 1 lstype 3 smtype 7 manrmsteps 1 miniter 1 rtolf 1e-6\n")

    # The analysis is set up for heat transfer, as there is some limitations to the, mathematically identical,
    # mass transport problem in OOFEM.
    f.write("vtkxml tstep_all cellvars 2 103 41 primvars 1 6 stype 0\n")  # Heat transfer
    # f.write("vtkxml tstep_all cellvars 2 103 58 primvars 1 7 stype 0\n")  # Mass transport
    f.write("domain 3d\n")
    f.write("outputmanager\n")
    f.write("ndofman {0} nelem {1} nset {2} ncrosssect {3} nmat {3} nbc 2 nltf 2 nic 0\n".format(
            NX*NY*NZ, M[0]*M[1]*M[2], nmat + 4, nmat))
    node = 0
    for nz in range(NX):
        for ny in range(NY):
            for nx in range(NZ):
                node += 1
                f.write("Node {} coords 3 {} {} {}\n".format(node, nx*spacing[0], ny*spacing[1], nz*spacing[2]))

    element = 0
    for ez in range(M[2]):
        for ey in range(M[1]):
            for ex in range(M[0]):
                element += 1
                nodes = np.array([NX*NY+NZ, NX*NY+NX+1, NX*NY + 1, NY*NX, NX, NX+1, 1, 0]) + (1+ex + NX*ey + NX*NY*ez)
                if phase_ids[ex + ey * M[0] + ez * M[0] * M[1]] == 1:
                    f.write("brick1ht {} nodes 8 {} {} {} {} {} {} {} {}\n".format(element, *nodes))
                else:
                    f.write("brick1ht {} nodes 8 {} {} {} {} {} {} {} {} activityltf 2\n".format(element, *nodes))

    # Sets:
    for setcount, phase in enumerate(ids_in_phase):
        f.write("Set {} elements {} ".format(setcount+1, len(phase)))
        f.write("".join(" {}".format(k+1) for k in phase))
        f.write("\n")

    # X-Y plane (facing -z):
    f.write("Set {} elementboundaries {} ".format(nmat + 1, 2*(M[0]*M[1] + M[0]*M[2] + M[1]*M[2])))
    ez = 0
    for ey in range(M[1]):
        for ex in range(M[0]):
            f.write(" {} 2".format(M[0]*M[1]*ez + M[0]*ey + ex + 1))
    # X-Z plane (facing -y):
    f.write("\\\n")
    for ez in range(M[2]):
        ey = 0
        for ex in range(M[0]):
            f.write(" {} 5".format(M[0]*M[1]*ez + M[0]*ey + ex + 1))
    # Y-Z plane (facing -x):
    f.write("\\\n")
    for ez in range(M[2]):
        for ey in range(M[1]):
            ex = 0
            f.write(" {} 6".format(M[0]*M[1]*ez + M[0]*ey + ex + 1))
    f.write("\n")

    f.write("Set {} elementboundaries {} ".format(nmat + 2, 2*(M[0]*M[1] + M[0]*M[2] + M[1]*M[2])))
    # X-Y plane (facing +z):
    ez = M[2]-1
    for ey in range(M[1]):
        for ex in range(M[0]):
            f.write(" {} 1".format(M[0]*M[1]*ez + M[0]*ey + ex + 1))
    # X-Z plane (facing +y):
    f.write("\\\n")
    for ez in range(M[2]):
        ey = M[1]-1
        for ex in range(M[0]):
            f.write(" {} 3".format(M[0]*M[1]*ez + M[0]*ey + ex + 1))
    # Y-Z plane (facing +x):
    f.write("\\\n")
    for ez in range(M[2]):
        for ey in range(M[1]):
            ex = M[0]-1
            f.write(" {} 4".format(M[0]*M[1]*ez + M[0]*ey + ex + 1))
    f.write("\n")

    # Prescribe nodes that are "loose" inside the domain:
    conodes = np.zeros(NX*NY*NZ, dtype=bool)
    for ez in range(M[2]):
        for ey in range(M[1]):
            for ex in range(M[0]):
                if phase_ids[M[0]*M[1]*ez + M[0]*ey + ex] == 1:
                    # Co phase, then mark conodes as true for all nodes:
                    nodes = np.array([NX*NY+NZ, NX*NY+NX+1, NX*NY + 1, NY*NX, NX, NX+1, 1, 0]) + (ex + NX*ey + NX*NY*ez)
                    conodes[nodes] = True
    # Special concern for b.c. If Dirichlet, then we must allow the boundary nodes to be free.
    # For periodic b.c. then we must mark the master surface (+z,+y,+x side) to Co nodes,
    # as well as free up the slaves (they follow the master anyway).
    # Here, only periodic is supported:
    # X-Y plane:
    nzm = 0
    nz = NZ-1
    for ny in range(NY):
        for nx in range(NX):
            if conodes[NX*NY*nz + NX*ny + nx]:
                conodes[NX*NY*nzm + NX*ny + nx] = True
    # X-Z plane:
    for nz in range(NZ):
        nym = 0
        ny = NY-1
        for nx in range(NX):
            if conodes[NX*NY*nz + NX*ny + nx]:
                conodes[NX*NY*nz + NX*nym + nx] = True
    # Y-Z plane:
    for nz in range(NZ):
        for ny in range(NY):
            nxm = 0
            nx = NX-1
            if conodes[NX*NY*nz + NX*ny + nx]:
                conodes[NX*NY*nz + NX*ny + nxm] = True

    nz = NZ-1
    for ny in range(NY):
        for nx in range(NX):
            conodes[NX*NY*nz + NX*ny + nx] = True
    for nz in range(NZ):
        ny = NY-1
        for nx in range(NX):
            conodes[NX*NY*nz + NX*ny + nx] = True
    for nz in range(NZ):
        for ny in range(NY):
            nx = NX-1
            conodes[NX*NY*nz + NX*ny + nx] = True

    prescribe = np.where(~conodes)[0]
    f.write("Set {} nodes {}  ".format(nmat + 3, len(prescribe)) + " ".join([str(x+1) for x in prescribe]) + "\n")

    prescribe = np.where(conodes)[0]
    f.write("Set {} nodes {}  ".format(nmat + 4, len(prescribe)) + " ".join([str(x+1) for x in prescribe]) + "\n")

    # Crosssections, maaterials, BCs and functions
    f.write("SimpleTransportCS {0} mat {0} set {0}\n".format(1))
    f.write("SimpleTransportCS {0} mat {0} set {0}\n".format(2))

    f.write("IsoHeat {} d 1. k {} c {}\n".format(1, k_Co, 1.0))
    f.write("IsoHeat {} d 1. k {} c {}\n".format(2, k_WC, 1.0))
    #f.write("isolinmoisturemat {} d 1. perm {} capa {}\n".format(1, k_Co, 1.0))
    #f.write("isolinmoisturemat {} d 1. perm {} capa {}\n".format(2, k_WC, 1.0))

    # f.write("TMGradDirichlet 1 loadTimeFunction 1 dofs 1 10 gradient 1 3 {{0. 0. 0.}} set {}\n".format(nmat+3))
    f.write("TMGradPeriodic 1 loadTimeFunction 1 dofs 1 10 gradient 3 0. 0. 1. " +
            "jump 3 {0} {1} {2} set {3} masterSet {4}\n".format(
                spacing[0]*M[0], spacing[0]*M[1], spacing[0]*M[2], nmat + 2, nmat + 1))
    #f.write("BoundaryCondition 1 loadTimeFunction 1 dofs 1 10 values 1 5.0 set {}\n".format(nmat + 4))
    # Precsribe all WC-nodes:
    f.write("BoundaryCondition 2 loadTimeFunction 1 dofs 1 10 values 1 0.0 set {}\n".format(0 if k_WC > 0 else nmat + 3))
    f.write("ConstantFunction 1 f(t) 1.0\n")
    f.write("ConstantFunction 2 f(t) 0.0\n")


def write_dream3d(filename, M, spacing, trunc_triangles, grain_ids, phases, good_voxels, euler_angles,
               surface_voxels=None, gb_voxels=None, interface_voxels=None, overlaps=None, compress=False):
    """
    Writes Dream3D file (using HDF5 format).
    """
    import h5py
    f = h5py.File(filename + '.dream3d', 'w')
    cmpr = 'gzip' if compress else None

    f.attrs.create("FileVersion", '7.0', shape=(1,), dtype='S4')
    f.attrs.create("DREAM3D Version", '6.2', shape=(1,), dtype='S4')

    grp_containers = f.create_group("DataContainerBundles")

    grp_containers = f.create_group("DataContainers")
    grp_voxel_data = grp_containers.create_group("VoxelDataContainer")

    grp_voxel_data.attrs.create("NUM_POINTS", np.array([M[0]*M[1]*M[2]]), shape=(1,), dtype='int64')
    grp_voxel_data.attrs.create("VTK_DATA_OBJECT", "VTK_STRUCTURED_POINTS", shape=(1,), dtype='S22')

    # Geometry group
    grp_simpl_geometry = grp_voxel_data.create_group("_SIMPL_GEOMETRY")
    grp_simpl_geometry.attrs.create("GeometryName", 'ImageGeometry', shape=(1,), dtype='S13')
    grp_simpl_geometry.attrs.create("GeometryTypeName", 'ImageGeometry', shape=(1,), dtype='S13')
    grp_simpl_geometry.attrs.create("GeometryType", (11,), shape=(1,), dtype='int64')
    grp_simpl_geometry.attrs.create("SpatialDimensionality", (3,), shape=(1,), dtype='int64')
    grp_simpl_geometry.attrs.create("UnitDimensionality", (3,), shape=(1,), dtype='int64')

    dset_dimensions = grp_simpl_geometry.create_dataset("DIMENSIONS", (3,), dtype='int64')
    dset_dimensions[...] = M

    dset_origin = grp_simpl_geometry.create_dataset("ORIGIN", (3,), dtype='float32')
    dset_origin[...] = np.array([0, 0, 0])

    dset_origin = grp_simpl_geometry.create_dataset("SPACING", (3,), dtype='float32')
    dset_origin[...] = spacing

    # Cell data group
    grp_cell_data = grp_voxel_data.create_group("CellData")
    grp_cell_data.attrs.create("AttributeMatrixType", np.array([3]), shape=(1,), dtype='int32')
    grp_cell_data.attrs.create("TupleDimensions", M, (3,), dtype="uint64")

    dset_grain_ids = grp_cell_data.create_dataset("GrainIds", (M[0], M[1], M[2]), dtype='int32', compression=cmpr)
    dset_grain_ids.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='uint64')
    dset_grain_ids.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
    dset_grain_ids.attrs.create("ObjectType", "DataArray<int32_t>", shape=(1,), dtype='S19')
    dset_grain_ids.attrs.create("TupleDimensions", M, shape=(3,), dtype='uint64')
    dset_grain_ids[...] = np.reshape(grain_ids, M)

    dset_phases = grp_cell_data.create_dataset("Phases", (M[0], M[1], M[2],), dtype='int32', compression=cmpr)
    dset_phases.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='uint64')
    dset_phases.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
    dset_phases.attrs.create("ObjectType", "DataArray<int32_t>", shape=(1,), dtype='S19')
    dset_phases.attrs.create("TupleDimensions", M, shape=(3,), dtype='uint64')
    dset_phases[...] = np.reshape(phases, M)

    dset_good_voxels = grp_cell_data.create_dataset("GoodVoxels", (M[0], M[1], M[2],), dtype='uint8', compression=cmpr)
    dset_good_voxels.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='uint64')
    dset_good_voxels.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
    dset_good_voxels.attrs.create("ObjectType", "DataArray<uint8_t>", shape=(1,), dtype='S16')
    dset_good_voxels.attrs.create("TupleDimensions", M, shape=(3,), dtype='uint64')
    dset_good_voxels[...] = np.reshape(good_voxels, M)

    dset_euler_angles = grp_cell_data.create_dataset("EulerAngles", (M[0], M[1], M[2], 3), dtype='float32', compression=cmpr)
    dset_euler_angles.attrs.create("ComponentDimensions", np.array([3]), shape=(1,), dtype='uint64')
    dset_euler_angles.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
    dset_euler_angles.attrs.create("ObjectType", "DataArray<float>", shape=(1,), dtype='S17')
    dset_euler_angles.attrs.create("TupleDimensions", M, shape=(3,), dtype='uint64')
    dset_euler_angles[...] = np.reshape(euler_angles, [M[0], M[1], M[2], 3])

    # Cell ensemble data
    grp_ensemble_data = grp_voxel_data.create_group("CellEnsembleData")
    grp_ensemble_data.attrs.create("AttributeMatrixType", np.array([11]), shape=(1,), dtype='uint64')
    grp_ensemble_data.attrs.create("Name", "EnsembleData", shape=(1,), dtype='S13')
    grp_ensemble_data.attrs.create("TupleDimensions", np.array([3]), (1,), dtype="uint64")

    dset_crystal_structs = grp_ensemble_data.create_dataset("CrystalStructures", (3,), dtype='uint32')
    dset_crystal_structs.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='uint64')
    dset_crystal_structs.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
    dset_crystal_structs.attrs.create("ObjectType", "DataArray<uint32_t>", shape=(1,), dtype='S20')
    dset_crystal_structs.attrs.create("TupleDimensions", np.array([3]), shape=(1,), dtype='uint64')
    dset_crystal_structs[...] = np.array([999, 1, 0])

    dset_phase_types = grp_ensemble_data.create_dataset("PhaseTypes", (3,), dtype='uint32')
    dset_phase_types.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='uint64')
    dset_phase_types.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
    dset_phase_types.attrs.create("ObjectType", "DataArray<uint32_t>", shape=(1,), dtype='S20')
    dset_phase_types.attrs.create("TupleDimensions", np.array([3]), shape=(1,), dtype='uint64')
    dset_phase_types[...] = np.array([999, 3, 1])


    if surface_voxels is not None:
        dset_surface_voxels = grp_cell_data.create_dataset("SurfaceVoxels", (M[0], M[1], M[2],), dtype='int8', compression=cmpr)
        dset_surface_voxels.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
        dset_surface_voxels.attrs.create("TupleDimensions", M, shape=(3,), dtype='uint64')
        dset_surface_voxels.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='uint64')
        dset_surface_voxels.attrs.create("ObjectType", "DataArray<int8_t>", shape=(1,), dtype='S18')
        dset_surface_voxels[...] = np.reshape(surface_voxels, M)
    if gb_voxels is not None:
        dset_gb_voxels = grp_cell_data.create_dataset("GrainBoundaryVoxels", (M[0], M[1], M[2],), dtype='int8', compression=cmpr)
        dset_gb_voxels.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
        dset_gb_voxels.attrs.create("TupleDimensions", M, shape=(3,), dtype='uint64')
        dset_gb_voxels.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='uint64')
        dset_gb_voxels.attrs.create("ObjectType", "DataArray<int8_t>", shape=(1,), dtype='S18')
        dset_gb_voxels[...] = np.reshape(gb_voxels, M)
    if interface_voxels is not None:
        dset_interface_voxels = grp_cell_data.create_dataset("InterfaceVoxels", (M[0], M[1], M[2],), dtype='int8', compression=cmpr)
        dset_interface_voxels.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
        dset_interface_voxels.attrs.create("TupleDimensions", M, shape=(3,), dtype='uint64')
        dset_interface_voxels.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='uint64')
        dset_interface_voxels.attrs.create("ObjectType", "DataArray<int8_t>", shape=(1,), dtype='S18')
        dset_interface_voxels[...] = np.reshape(interface_voxels, M)
    if overlaps is not None:
        dset_overlaps = grp_cell_data.create_dataset("Overlaps", (M[0], M[1], M[2],), dtype='int8', compression=cmpr)
        dset_overlaps.attrs.create("DataArrayVersion", np.array([2]), shape=(1,), dtype='int32')
        dset_overlaps.attrs.create("TupleDimensions", M, shape=(3,), dtype='uint64')
        dset_overlaps.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='uint64')
        dset_overlaps.attrs.create("ObjectType", "DataArray<int8_t>", shape=(1,), dtype='S18')
        dset_overlaps[...] = np.reshape(overlaps, M)

    """
    # Cell data group
    grp_field_data = grp_voxel_data.create_group("FieldData")
    grp_field_data.attrs.create("Name", "FieldData", shape=(1,), dtype='S10')

    phases2 = np.zeros(len(trunc_triangles)+2, dtype='int32')
    phases2[1] = 1
    phases2[2:] = 2
    dset_phases2 = grp_field_data.create_dataset("Phases", (len(phases2),), dtype='int32')
    dset_phases2.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='int32')
    dset_phases2.attrs.create("ObjectType", "DataArray<int32_t>", shape=(1,), dtype='S19')
    dset_phases2[...] = phases2

    active = np.ones(len(trunc_triangles)+2, dtype='uint8')
    active[0] = 0
    dset_active = grp_field_data.create_dataset("Active", (len(active),), dtype='uint8')
    dset_active.attrs.create("ComponentDimensions", np.array([1]), shape=(1,), dtype='int32')
    dset_active.attrs.create("ObjectType", "DataArray<bool>", shape=(1,), dtype='S16')
    dset_active[...] = active

    euler_angles_t = np.array([trunc_triangle.euler_angles for trunc_triangle in trunc_triangles])
    euler_angles2 = np.zeros((len(euler_angles_t)+2, 3))
    euler_angles2[2:] = euler_angles_t
    dset_euler_angles2 = grp_field_data.create_dataset("EulerAngles", (len(euler_angles2), 3), dtype='float32')
    dset_euler_angles2.attrs.create("ComponentDimensions", np.array([3]), shape=(1,), dtype='int32')
    dset_euler_angles2.attrs.create("ObjectType", "DataArray<float>", shape=(1,), dtype='S17')
    dset_euler_angles2[...] = euler_angles2

    dset_phases = grp_cell_data.create_dataset("Phases", (M[0], M[1], M[2],), dtype='int32')
    """

    pipeline = f.create_group("Pipeline")
    pipeline.attrs.create("Number_Filters", np.array([0]), shape=(1,), dtype='int32')

    f.close()

    write_xdmf(filename + '.xdmf', M, spacing, surface_voxels is not None, gb_voxels is not None,
               interface_voxels is not None, overlaps is not None)


def write_xdmf(filename, M, spacing, surface_voxels=False, gb_voxels=False, interface_voxels=False, overlaps=False):
    with open(filename, 'w') as f:
        filename_hdf5 = filename[:filename.rfind('.')].split('/')[-1] + ".dream3d"
        M_plus1 = [M[0]+1, M[1]+1, M[2]+1]

        f.write('<?xml version="1.0"?>\n')
        f.write('<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd"[]>\n')
        f.write('<Xdmf xmlns:xi="http://www.w3.org/2003/XInclude" Version="2.2">\n')
        f.write(' <Domain>\n\n')
        f.write('  <Grid Name="Cell Data" GridType="Uniform">\n')
        f.write('    <Topology TopologyType="3DCoRectMesh" Dimensions="{} {} {} "></Topology>\n'.format(*M_plus1))
        f.write('    <Geometry Type="ORIGIN_DXDYDZ">\n')
        f.write('      <!-- Origin -->\n')
        f.write('      <DataItem Format="XML" Dimensions="3">0 0 0</DataItem>\n')
        f.write('      <!-- DxDyDz (Spacing/Resolution)-->\n')
        f.write('      <DataItem Format="XML" Dimensions="3">{:g} {:g} {:g}</DataItem>\n'.format(*spacing))
        f.write('    </Geometry>\n')
        f.write('    <Attribute Name="EulerAngles (Cell)" AttributeType="Vector" Center="Cell">\n')
        f.write('      <DataItem Format="HDF" Dimensions="{} {} {} 3 " NumberType="Float" Precision="4" >\n'.format(*M))
        f.write('        {}:/DataContainers/VoxelDataContainer/CellData/EulerAngles\n'.format(filename_hdf5))
        f.write('      </DataItem>\n')
        f.write('    </Attribute>\n')
        f.write('\n')
        f.write('    <Attribute Name="GoodVoxels (Cell)" AttributeType="Scalar" Center="Cell">\n')
        f.write('      <DataItem Format="HDF" Dimensions="{} {} {} " NumberType="uchar" Precision="1" >\n'.format(*M))
        f.write('        {}:/DataContainers/VoxelDataContainer/CellData/GoodVoxels\n'.format(filename_hdf5))
        f.write('      </DataItem>\n')
        f.write('    </Attribute>\n')
        f.write('\n')
        f.write('    <Attribute Name="GrainIds (Cell)" AttributeType="Scalar" Center="Cell">\n')
        f.write('      <DataItem Format="HDF" Dimensions="{} {} {} " NumberType="Int" Precision="4" >\n'.format(*M))
        f.write('        {}:/DataContainers/VoxelDataContainer/CellData/GrainIds\n'.format(filename_hdf5))
        f.write('      </DataItem>\n')
        f.write('    </Attribute>\n')
        f.write('\n')
        f.write('    <Attribute Name="Phases (Cell)" AttributeType="Scalar" Center="Cell">\n')
        f.write('      <DataItem Format="HDF" Dimensions="{} {} {} " NumberType="Int" Precision="4" >\n'.format(*M))
        f.write('        {}:/DataContainers/VoxelDataContainer/CellData/Phases\n'.format(filename_hdf5))
        f.write('      </DataItem>\n')
        f.write('    </Attribute>\n')
        f.write('\n')
        if surface_voxels is not None:
            f.write('    <Attribute Name="SurfaceVoxels (Cell)" AttributeType="Scalar" Center="Cell">\n')
            f.write('      <DataItem Format="HDF" Dimensions="{} {} {} " NumberType="Char" Precision="1" >\n'.format(*M))
            f.write('        {}:/DataContainers/VoxelDataContainer/CellData/SurfaceVoxels\n'.format(filename_hdf5))
            f.write('      </DataItem>\n')
            f.write('    </Attribute>\n')
            f.write('\n')
        if gb_voxels is not None:
            f.write('    <Attribute Name="GrainBoundaryVoxels (Cell)" AttributeType="Scalar" Center="Cell">\n')
            f.write('      <DataItem Format="HDF" Dimensions="{} {} {} " NumberType="Char" Precision="1" >\n'.format(*M))
            f.write('        {}:/DataContainers/VoxelDataContainer/CellData/GrainBoundaryVoxels\n'.format(filename_hdf5))
            f.write('      </DataItem>\n')
            f.write('    </Attribute>\n')
            f.write('\n')
        if interface_voxels is not None:
            f.write('    <Attribute Name="InterfaceVoxels (Cell)" AttributeType="Scalar" Center="Cell">\n')
            f.write('      <DataItem Format="HDF" Dimensions="{} {} {} " NumberType="Char" Precision="1" >\n'.format(*M))
            f.write('        {}:/DataContainers/VoxelDataContainer/CellData/InterfaceVoxels\n'.format(filename_hdf5))
            f.write('      </DataItem>\n')
            f.write('    </Attribute>\n')
            f.write('\n')
        if overlaps is not None:
            f.write('    <Attribute Name="Overlaps (Cell)" AttributeType="Scalar" Center="Cell">\n')
            f.write('      <DataItem Format="HDF" Dimensions="{} {} {} " NumberType="Char" Precision="1" >\n'.format(*M))
            f.write('        {}:/DataContainers/VoxelDataContainer/CellData/Overlaps\n'.format(filename_hdf5))
            f.write('      </DataItem>\n')
            f.write('    </Attribute>\n')
            f.write('\n')

        f.write('  </Grid>\n')
        f.write('    <!-- *************** END OF Cell Data *************** -->\n')
        f.write('\n')
        f.write(' </Domain>\n')
        f.write('</Xdmf>\n')
        f.close()


def read_dream3d(filename):
    import h5py
    f = h5py.File(filename + '.dream3d', 'r')

    # To return:
    # M, spacing, trunc_triangles, grain_ids, phases, good_voxels, euler_angles, overlaps=None
    grp_voxel_data = f['DataContainers']['VoxelDataContainer']
    grp_simpl_geometry = grp_voxel_data['_SIMPL_GEOMETRY']
    M = grp_simpl_geometry['DIMENSIONS'][...]
    spacing = grp_simpl_geometry['SPACING'][...]

    grp_cell_data = grp_voxel_data['CellData']
    grain_ids = grp_cell_data['GrainIds'][...]
    phases = grp_cell_data['Phases'][...]
    good_voxels = grp_cell_data['GoodVoxels'][...]
    euler_angles = grp_cell_data['EulerAngles'][...]
    if 'Overlaps' in grp_cell_data:
        overlaps = grp_cell_data['Overlaps'][...]

    f.close()

    return M, spacing, grain_ids, phases, good_voxels, euler_angles, overlaps
