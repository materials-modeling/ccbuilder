#!/usr/bin/env python3
from __future__ import print_function
from __future__ import division
import CCBuilder as ccb
import CCBuilder_c as ccb_c
from misorientation import *
import sys
import numpy as np
import scipy.special


def uniform_dist(x):
    """ Returns uniform distributions of given range """
    return lambda: np.random.uniform(x[0], x[1])


def weibull_dist(a, mu):
    """ Returns Weibull distributions for given shape parameter and average """
    return lambda: np.random.weibull(a) * mu / scipy.special.gamma(1/a + 1)


def parse_dist(arg):
    # Parses input string for given distribution.
    # Returns a distribution, and the average
    d, params = arg.split(':')
    params = [float(x) for x in params.split(',')]
    if d == 'U':
        return uniform_dist(params), np.mean(params)
    elif d == 'W':
        a, mu = params
        return weibull_dist(a, mu), mu

# Controlled randomness (for repeatability)
# np.random.seed(0)

# Cube size:
L = float(10)

# Grain size:
d_0 = 1  # 1 micron average grain size (length scale) (should reflect the average grain size)
d_eq = uniform_dist([d_0 - 0.5, d_0 + 0.5])
# d_eq = uniform_dist([d_0, d_0])
# d_eq = weibull_dist(4, d_0)

# Grain shape:
r = uniform_dist([0.1, 0.4])
# k = uniform_dist([0.1, 0.4])  # low k (C rich)
k = uniform_dist([0.4, 1.4])  # high k (W rich)

# Target volume fraction:
vol_frac_goal = 1.0

# Final discretization density:
ms = np.array([5, 10, 15, 20, 25])  # given as "voxels per d_0"
#ms = np.array([20])  # given as "voxels per d_0"

# Number of Monte Carlo iterations (scaled with M^4)
mc_steps = 0.1
# Fictitious temperature for Potts
kBT = 0.5

# Number of random translations to try
nr_tries = int(sys.argv[1])
use_potential = bool(int(sys.argv[2]))
if nr_tries == 0:
    target_fracs = np.linspace(0.01, 2.0, 15)
else:
    target_fracs = np.linspace(0.01, 1.4, 15)

# How many samples to run
samples = 5

co_fracs_mean = []
co_fracs_std = []

cont_mean = []
cont_std = []
cont_potts_mean = []
cont_potts_std = []

d_eq_mean_mean = []
d_eq_mean_std = []
d_eq_potts_mean_mean = []
d_eq_potts_mean_std = []

for m in ms:

    contiguity_0 = []
    contiguity_2 = []
    vol_frac_Co_0 = []
    d_eq_0 = []
    d_eq_2 = []

    for sample in range(samples):
        np.random.seed(sample)

        # to avoid confusion
        vol_frac_goal = np.double(vol_frac_goal)
        L = np.double(L)
        kBT = np.double(kBT)
        nr_tries = np.int(nr_tries)
        delta_x = float(d_0)/float(m)
        M = np.int(m * L / d_0)

        print("Running vol fraction", vol_frac_goal, "with M", M)
        fname = 'mstat_{:.1f}_{}'.format(vol_frac_goal, sample)

        trunc_triangles = ccb.prepare_triangles(vol_frac_goal, L, r, k, d_eq)
        trunc_triangles.sort(key=lambda m: m.volume, reverse=True)
        print("Prepared", len(trunc_triangles), "triangles")
        if use_potential:
            ccb.optimize_midpoints(L, trunc_triangles)
            grain_ids_0, overlaps_0, voxel_indices_0 = ccb_c.populate_voxels(M, L, trunc_triangles, 1, 0, 1.0)
        else:
            grain_ids_0, overlaps_0, voxel_indices_0 = ccb_c.populate_voxels(M, L, trunc_triangles, nr_tries, M, 1.0)
        phases_0, good_voxels_0, euler_angles_0, phase_volumes_0, grain_volumes_0 = ccb_c.calc_grain_prop(M, grain_ids_0, trunc_triangles)
        surface_voxels_0, gb_voxels_0, interface_voxels_0 = ccb_c.calc_surface_prop(M, grain_ids_0)

        vol_frac_WC_0 = phase_volumes_0[1]/np.float(np.sum(phase_volumes_0))
        vol_frac_Co_0.append( 1 - vol_frac_WC_0 )

        sum_gb_voxels_0 = np.sum(gb_voxels_0)
        contiguity_0.append( sum_gb_voxels_0 / np.float(sum_gb_voxels_0 + np.sum(interface_voxels_0)) )

        d_eq_0.append(np.mean(np.cbrt(6./np.pi * grain_volumes_0 * ((L/M)**3))))

        print("********************** Final vol fraction", vol_frac_Co_0[-1])

        # Do Potts on coarse grid first for an improved initial guess.
        grain_ids_2 = grain_ids_0.copy()
        gb_voxels_2 = gb_voxels_0.copy()

        if mc_steps > 0:
            M_coarse = M//2
            print("creating coarse grid")
            grain_ids_coarse, overlaps_coarse, voxel_indices_coarse = ccb_c.populate_voxels(M_coarse, L, trunc_triangles, 0, M_coarse, 1.0)
            print("coarse grid for potts done")
            surface_voxels_coarse, gb_voxels_coarse, interface_voxels_coarse = ccb_c.calc_surface_prop(M_coarse, grain_ids_coarse)
            print("starting coarse potts")
            ccb_c.make_mcp_bound(M_coarse, grain_ids_coarse, gb_voxels_coarse, overlaps_coarse, voxel_indices_coarse, np.int(mc_steps*M_coarse**4), kBT)
            print("finished coarse potts")
            #
            # Copy over that solution to the overlap regions of the fine grid as a starting point
            i = np.nonzero(overlaps_0)[0]
            iz = i // (M*M)
            iy = (i - iz*M*M) // M
            ix = i - iz*M*M - iy*M
            cix, ciy, ciz = ix * M_coarse // M, iy * M_coarse // M, iz * M_coarse // M
            ci = cix + ciy*M_coarse + ciz*M_coarse**2
            # For a very coarse grid, we should check the voxel indices. Requires Cython implementation for efficiency.
            gid = grain_ids_coarse[ci]
            for ii, g in zip(i, gid):
                if g != grain_ids_2[ii] and np.searchsorted(voxel_indices_0[g-2], ii) < len(voxel_indices_0[g-2]):
                    grain_ids_2[ii] = g
            # This might change a few voxels to a value that they shouldn't obtain, but it's barely noticeable
            #grain_ids_2[i] = grain_ids_coarse[ci]
            surface_voxels_2, gb_voxels_2, interface_voxels_2 = ccb_c.calc_surface_prop(M, grain_ids_2)
            ccb_c.make_mcp_bound(M, grain_ids_2, gb_voxels_2, overlaps_0, voxel_indices_0, np.int(mc_steps*M**4), kBT)
            phases_2, good_voxels_2, euler_angles_2, phase_volumes_0, grain_volumes_2 = ccb_c.calc_grain_prop(M, grain_ids_2, trunc_triangles)
        else:
            surface_voxels_2, gb_voxels_2, interface_voxels_2 = surface_voxels_0, gb_voxels_0, interface_voxels_0
            phases_2, good_voxels_2, euler_angles_2, phase_volumes_2, grain_volumes_2 = phases_0, good_voxels_0, euler_angles_0, phase_volumes_0, grain_volumes_0

        sum_gb_voxels_2 = np.sum(gb_voxels_2)
        contiguity_2.append( sum_gb_voxels_2 / np.float(sum_gb_voxels_2 + np.sum(interface_voxels_2)) )

        d_eq_2.append(np.mean(np.cbrt(6./np.pi * grain_volumes_2 * ((L/M)**3))))

        #ccb.write_hdf5(fname + '.dream3d', 3*[M], 3*[delta_x], trunc_triangles, grain_ids_2, phases_2, good_voxels_2,
        #               euler_angles_2, surface_voxels_2, gb_voxels_2, interface_voxels_2, overlaps_0)
        #ccb.write_oofem_transport(fname + "_tm", 3*[M], 3*[delta_x], phases_2)

    co_fracs_mean.append(np.mean(vol_frac_Co_0))
    co_fracs_std.append(np.std(vol_frac_Co_0))

    cont_mean.append(np.mean(contiguity_0))
    cont_std.append(np.std(contiguity_0))
    cont_potts_mean.append(np.mean(contiguity_2))
    cont_potts_std.append(np.std(contiguity_2))

    d_eq_mean_mean.append(np.mean(d_eq_0))
    d_eq_mean_std.append(np.std(d_eq_0))
    d_eq_potts_mean_mean.append(np.mean(d_eq_2))
    d_eq_potts_mean_std.append(np.std(d_eq_2))

if False:
    import matplotlib.pyplot as plt
    plt.plot(ms, cont_potts_mean)
    plt.plot(ms, cont_mean)
    plt.show()

fname = 'stat_m'
if use_potential:
    fname += '_U'
elif nr_tries == 0:
    fname += '_random'
else:
    fname += '_transl'

print(cont_mean, cont_potts_mean)

np.savetxt(fname + "_L{}.txt".format(L), np.transpose([ms, co_fracs_mean, co_fracs_std,
                                                       cont_mean, cont_std, cont_potts_mean, cont_potts_std,
                                                       d_eq_mean_mean, d_eq_mean_std,
                                                       d_eq_potts_mean_mean, d_eq_potts_mean_std]),
           header='m co_fracs_mean co_fracs_std cont_mean cont_std cont_potts_mean cont_potts_std ' +
                  'd_eq_mean_mean d_eq_mean_std d_eq_potts_mean_mean d_eq_potts_mean_std',
           comments='')
