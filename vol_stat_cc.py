#!/usr/bin/env python3
from __future__ import print_function
from __future__ import division
import argparse
import pickle
import time
import CCBuilder as ccb
import CCBuilder_c as ccb_c
import numpy as np
import scipy.special

def uniform_dist(x):
    """ Returns uniform distributions of given range """
    return lambda: np.random.uniform(x[0], x[1])


def weibull_dist(a, mu):
    """ Returns Weibull distributions for given shape parameter and average """
    return lambda: np.random.weibull(a) * mu / scipy.special.gamma(1/a + 1)


def parse_dist(arg):
    # Parses input string for given distribution.
    # Returns a distribution, and the average
    d, params = arg.split(':')
    params = [float(x) for x in params.split(',')]
    if d == 'U':
        return uniform_dist(params), np.mean(params)
    elif d == 'W':
        a, mu = params
        return weibull_dist(a, mu), mu


parser = argparse.ArgumentParser(description='''Generate a WC microstructure.
Grain shape/size supports 2 types of distributions:
Uniform: U:low,high
Weibull: U:a,mu     (a=k in some notation, mu=mean)
''',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

# parser.add_argument('-V', dest='verbose', action='store_true', help='Verbose mode.')

parser.add_argument('-f', dest='fname', metavar='basename', required=True, help='Output base filename.')

parser.add_argument('-L', dest='L', metavar='length', required=True, type=float, help='Cell length (volume is L^3)')

parser.add_argument('-m', dest='m', metavar='m', required=True, type=int,
                    help='Grid resolution. Total number of voxels are (m*L)^3')

#parser.add_argument('--vol_frac_goal', dest="vol_frac_goal", metavar='v', required=True, type=float,
#                    help='Goal for volume fraction WC (excluding overlap)')

parser.add_argument('-s', dest='seed', metavar='s', default=None, type=int,
                    help='Seed for RNG. Given identical parameters, ' +
                         'CCBuilder will generate identical output given a controlled seed.')

parser.add_argument('--samples', dest='samples', metavar='s', required=True, type=int,
                    help='Number of samples per volume fraction')

parser.add_argument('--stray_cleanup', action='store_true', help='Clean up stray voxels')

group = parser.add_argument_group('WC grain shape')
group.add_argument('-k', dest='k_dist', metavar='type,[params]', default='U:0.4,1.4',
                   help='k distribution')
group.add_argument('-r', dest='r_dist', metavar='type,[params]', default='U:0.1,0.4',
                   help='r distribution')
group.add_argument('-d', dest='d_dist', metavar='type,[params]', default='U:0.5,1.5',
                   help='d distribution')

group = parser.add_argument_group('Packing')
group.add_argument('--use_potential', action='store_true', help='Use repulsive potential.')
group.add_argument('--nr_tries', dest='nr_tries', metavar='n', default=2500, type=int,
                   help='Number of random translations.')
group.add_argument('--delta', dest='delta', metavar='d', type=float,
                   help='Maximum distance for randomized translations.')
group.add_argument('--m_coarse', dest="m_coarse", metavar='mc', default=10,
                   help='Grid resolution during packing.')

group = parser.add_argument_group('Potts simulation')
group.add_argument('--mc_steps', dest="mc_steps", metavar='steps', default=0.05, type=float,
                   help='Monte-Carlo steps (scales with (m*L)^4. Set to zero to turn off.')
group.add_argument('--tau', dest='tau', metavar='t', default=0.5, type=float,
                   help='Ficticious temperature in Potts model.')

options = parser.parse_args()

if options.seed is not None:
    np.random.seed(options.seed)

# Heuristic mapping from actual to goal volume fraction
# vol_frac_goal = (alpha - 2)/(2 * alpha) + 1/alpha * np.sqrt(1 - alpha * np.log(-2*(vol_frac - 1)))

d_eq, d_0 = parse_dist(options.d_dist)
r, r_0 = parse_dist(options.r_dist)
k, k_0 = parse_dist(options.k_dist)

fname = options.fname

# to avoid confusion with types:
m = np.int(options.m)
m_coarse = np.int(options.m_coarse)
L = np.float(options.L)
mc_steps = np.float(options.mc_steps)
#vol_frac_goal = np.double(options.vol_frac_goal)
tau = np.double(options.tau)
nr_tries = np.int(options.nr_tries)
delta_x = d_0/float(m)
M = np.int(m * L / d_0)
M_coarse = np.int(m_coarse * L / d_0)

idelta = M
idelta_coarse = M_coarse
if options.delta:
    idelta = np.int(M * options.delta / L)
    idelta_coarse = np.int(M_coarse * options.delta / L)

if nr_tries == 0:
    target_fracs = np.linspace(0.01, 2.0, 15)
else:
    #target_fracs = np.linspace(0.01, 2.0, 20)
    target_fracs = np.linspace(0.1, 1.4, 14)
    #target_fracs = np.linspace(1.0, 1.3, 4)

co_fracs_mean = []
co_fracs_std = []

cont_mean = []
cont_std = []
cont_potts_mean = []
cont_potts_std = []

d_eq_mean = []
d_eq_std = []
d_eq_potts_mean = []
d_eq_potts_std = []

for vol_frac_goal in target_fracs:
    vol_frac_goal = np.double(vol_frac_goal)
    contiguity_0 = []
    contiguity_2 = []
    vol_frac_Co = []

    for sample in range(options.samples):
        np.random.seed(sample)

        print("Running vol fraction", vol_frac_goal)

        trunc_triangles = ccb.prepare_triangles(vol_frac_goal, L, r, k, d_eq)
        trunc_triangles.sort(key=lambda m: m.volume, reverse=True)
        print("Prepared", len(trunc_triangles), "triangles")
        if options.use_potential:
            ccb.optimize_midpoints(L, trunc_triangles)
        if nr_tries > 1:
            grain_ids_coarse_0, overlaps_coarse_0, voxel_indices_coarse_0 = ccb_c.populate_voxels(M_coarse, L, trunc_triangles, nr_tries, M_coarse, 1.)
        grain_ids_0, overlaps_0, voxel_indices_0 = ccb_c.populate_voxels(M, L, trunc_triangles, 1, 0, 1.)

        phases_0, good_voxels_0, euler_angles_0 = ccb_c.calc_grain_prop(M, grain_ids_0, trunc_triangles)
        surface_voxels_0, gb_voxels_0, interface_voxels_0 = ccb_c.calc_surface_prop(M, grain_ids_0)

        phase_volumes_0 = np.bincount(phases_0)
        vol_frac_WC = phase_volumes_0[2] / np.float(M ** 3)
        vol_frac_Co.append( 1 - vol_frac_WC )
        print("********************** Final vol fraction", vol_frac_WC)

        sum_gb_voxels_0 = np.sum(gb_voxels_0)
        contiguity_0.append( sum_gb_voxels_0 / np.float(sum_gb_voxels_0 + np.sum(interface_voxels_0)) )

        grain_ids_2 = grain_ids_0.copy()
        gb_voxels_2 = gb_voxels_0.copy()

        # Do Potts on coarse grid first for an improved initial guess.
        if mc_steps > 0:
            M_coarseMC = M//2
            grain_ids_coarse, overlaps_coarse, voxel_indices_coarse = ccb_c.populate_voxels(M_coarseMC, L, trunc_triangles, 0, M_coarseMC, 1.)
            surface_voxels_coarse, gb_voxels_coarse, interface_voxels_coarse = ccb_c.calc_surface_prop(M_coarseMC, grain_ids_coarse)
            ccb_c.make_mcp_bound(M_coarseMC, grain_ids_coarse, gb_voxels_coarse, overlaps_coarse, voxel_indices_coarse, np.int(mc_steps*M_coarseMC**4), tau)
            #
            # Copy over that solution to the overlap regions of the fine grid as a starting point
            i = np.nonzero(overlaps_0)[0]
            iz = i // (M*M)
            iy = (i - iz*M*M) // M
            ix = i - iz*M*M - iy*M
            cix, ciy, ciz = ix * M_coarseMC // M, iy * M_coarseMC // M, iz * M_coarseMC // M
            ci = cix + ciy*M_coarseMC + ciz*M_coarseMC**2
            # For a very coarse grid, we should check the voxel indices. Requires Cython implementation for efficiency.
            gid = grain_ids_coarse[ci]
            for ii, g in zip(i, gid):
                if g != grain_ids_2[ii] and np.searchsorted(voxel_indices_0[g-2], ii) < len(voxel_indices_0[g-2]):
                    grain_ids_2[ii] = g
            # This might change a few voxels to a value that they shouldn't obtain, but it's barely noticeable
            #grain_ids_2[i] = grain_ids_coarse[ci]
            surface_voxels_2, gb_voxels_2, interface_voxels_2 = ccb_c.calc_surface_prop(M, grain_ids_2)
            ccb_c.make_mcp_bound(M, grain_ids_2, gb_voxels_2, overlaps_0, voxel_indices_0, np.int(mc_steps*M**4), tau)
            phases_2, good_voxels_2, euler_angles_2 = ccb_c.calc_grain_prop(M, grain_ids_2, trunc_triangles)
        else:
            surface_voxels_2, gb_voxels_2, interface_voxels_2 = surface_voxels_0, gb_voxels_0, interface_voxels_0
            phases_2, good_voxels_2, euler_angles_2 = phases_0, good_voxels_0, euler_angles_0

        sum_gb_voxels_2 = np.sum(gb_voxels_2)
        contiguity_2.append( sum_gb_voxels_2 / np.float(sum_gb_voxels_2 + np.sum(interface_voxels_2)) )

        #d_eq_2.append(np.mean(np.cbrt(6./np.pi * grain_volumes_2 * ((L/M)**3))))

        #ccb.write_hdf5(fname + '.dream3d', 3*[M], 3*[delta_x], trunc_triangles, grain_ids_2, phases_2, good_voxels_2,
        #               euler_angles_2, surface_voxels_2, gb_voxels_2, interface_voxels_2, overlaps_0)
        #ccb.write_oofem_transport(fname + "_tm", 3*[M], 3*[delta_x], phases_2)

    co_fracs_mean.append(np.mean(vol_frac_Co))
    co_fracs_std.append(np.std(vol_frac_Co))

    cont_mean.append(np.mean(contiguity_0))
    cont_std.append(np.std(contiguity_0))
    cont_potts_mean.append(np.mean(contiguity_2))
    cont_potts_std.append(np.std(contiguity_2))

print(co_fracs_mean)

np.savetxt(fname + '.txt', np.transpose([target_fracs, co_fracs_mean, co_fracs_std,
                                cont_mean, cont_std, cont_potts_mean, cont_potts_std]),
           header='target_frac co_fracs_mean co_fracs_std ' +
                  'cont_mean cont_std cont_potts_mean cont_potts_std ',
           comments='')

if False:
    import matplotlib.pyplot as plt
    alpha = 1.3
    plt.plot(co_fracs_mean, cont_mean)
    plt.plot(co_fracs_mean, cont_potts_mean)
    y = 1. - 0.5*np.exp(- 2*(target_fracs-1/2) - alpha*(target_fracs-1/2)**2)
    y[target_fracs<0.5] = target_fracs[target_fracs<0.5]
    plt.plot(target_fracs, y, label='Fit')
    plt.plot(target_fracs, 1 - np.array(co_fracs_mean), label='Data')
    plt.show()
