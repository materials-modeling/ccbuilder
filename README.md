# CCBuilder

## Introduction
This is a short description of the effort to create a computer model of a three-dimensional (3D) microstructure
resembling typical microstructures of fully-dense conventionally sintered WC-Co cemented carbides.
The model is supposed to be used in finite-element method (FEM) modeling of WC-Co with cohesive zone models
for WC/WC and WC/Co boundaries.
The aim is to formulate a model that adequately reproduces the most important microstructural parameters in the WC-Co system.
These include the volume fractions of the respective phases, representative WC grain shapes, WC grain size distributions,
and the contiguity of the carbide phase.


Dream3D is a promising piece of software as it includes e.g. functionality for surface meshing.
Therefore, the output data structure of CCBuilder is made fully compatible with Dream3D data files to allow
importing data into Dream3D for further processing.

If you use CCBuilder in your research please include the following citation in publications or presentations:

* Sven A. E. Johansson, Mikael Öhman, Magnus Ekh, and Göran Wahnström, *CCBuilder, a software that produces synthetic microstructures of WC-Co cemented carbides*, Int. J. Refract. Met. Hard Mater. **78**, 210 (2019), doi: 10.1016/j.ijrmhm.2018.09.011

## How to build
CCBuilder is written in python with the computational expensive functions implemented in Cython.

To build CCbuilder run 

`python3 setup.py build_ext --inplace`

 and then you should be able to run the code

`python3 make_cc.py -h`


## Input parameters
There is a number of input parameters that need to be set when CCbuilder:

* Volume fraction goal, `vol_frac_goal`
* System size, `L` (should be chosen in relation to average grain size)
* Grid resolution (per grain), `m`

The volume fraction goal value does not account for overlapping grains,
and the final volume fraction will be signifanctly less.
One can use the formula:

 `vol_frac_goal = (alpha - 2)/(2 * alpha) + 1/alpha * np.sqrt(1 - alpha * np.log(-2*(vol_frac - 1)))`

where `alpha` depends on grain shape and size.
A value `alpha = 1.3` is suitable for the default values.
 
A larger system size will give less variation in the final volume fraction.



### Grain shape and size
Default values give realistic grains, but should be modified if a specific microstructure is desired.
* Prism truncation, `r`
* Grain height ratio, `k`
* Grain size, `d`

CCBuilder doesn't know about units, and `d` should be chosen in relation to `L`.
Support for Uniform and Weibull distributions are built in.

## Packing parameters
Packing parameters are also set to by default, but can be modified
* Repulsive potential, (on or off).
* Random translations:
  * Number of trials for placing a grain onto the grid, `nr_tries`
  * Maximum allowed translation for grid placement, `delta`
  * Grid resolution, `m_coarse`
Default values give a high packing factor, which is found to give the most realistic WC-systems.
The grid-resolution `m_coarse` allows for decoupling the resolution used during packing from the final resolution.
This is typically chosen as coarser than the final resolution to speed up algorithm.
One should use `m_coarse` >= 10 to fully capture the grain shape.

Potts model (Monte Carlo simulation for minimizing WC-WC grain boundaries).
* Number of MC steps, `mc_steps`
* Ficticious temperature, `tau`
Choosing mc_steps = 0 skips the Potts model completely.

There is also an option to turn on a cleanup of stray voxels which may occur a thin sections of Co.


## Additional tools

* `write_oofem.py` - Writes input files for analysis in OOFEM.
* `compute_misorientation.py` - Computes the misorientation of a
* `compute_co_grain_sizes.py` - Computes continuous Co regions


## CCBuilder library
CCBuilder is written as a library, with the core writting in Cython.
The core components of `make_cc.py` first makes use of

`ccb.prepare_triangles(vol_frac_goal, L r, k, d_eq)`

where the position, size, shape and orientation of each grain is drawn from random distributions.
Then optionally the midpoints of the grains can be optimized, ie trying to separate them as much as possible, by 

`ccb.optimize_midpoints(L, trunc_triangles)`

which leads to a better packing of the grains. 
Next the voxels are populate meaning each voxel is assigned to a grain or the binder by

`ccb_c.populate_voxels(M, L, trunc_triangles, nr_tries, idelta, stop_fraction)`

where a packed, discretized microstructure is generated.
